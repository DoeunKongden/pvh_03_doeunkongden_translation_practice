import i18next from "i18next";
import { initReactI18next } from "react-i18next";

//import the json of language 
import en from './language/en.json'
import jp from './language/jp.json'

i18next.use(initReactI18next).init({

    //for defining the resources : resources of the language translation
    resources: {
        en: {
            translation: en,
        },
        jp: {
            translation: jp,
        }
    },

    //for defining the language
    //also for defining of the default language
    //rather then we reading the default key value as example below
    // lng: "jp",

    //we can do it more dynamic
    lng: localStorage.getItem("lng") || "en"
})


export default i18next;