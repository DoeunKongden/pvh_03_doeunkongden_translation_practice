import { useTranslation } from "react-i18next";


function App() {

  //t key : is use for interact with the translation
  const { t, i18n } = useTranslation();


  const handleChangeLng = (lng) => {
    i18n.changeLanguage(lng);
    localStorage.setItem("lng",lng);
  }

  return (
    <div className="App">

      <button onClick={() => handleChangeLng("en")}>English</button>
      <button onClick={() => handleChangeLng("jp")}>Japan</button>

      <h1>{t("hello")}</h1>
      <h1>{t("how are you")}</h1>
    </div>
  );
}

export default App;
